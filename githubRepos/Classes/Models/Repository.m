//
//  Repository.m
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import "Repository.h"

@implementation Repository

- (instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (!self) return nil;
    
    _name = [dict[@"name"] copy];
    
    if ([dict[@"description"] isKindOfClass:[NSNull class]]) {
        _info = @"";
    } else {
        _info = [dict[@"description"] copy];
    }
    
    _ownerName = [dict[@"owner"][@"login"] copy];
    _ownerIcon = [dict[@"owner"][@"avatar_url"] copy];
    
    _branch = [dict[@"branches_url"] copy];
    _commit = [dict[@"commits_url"] copy];
    _hashCommit = [dict[@"node_id"] copy];
    
    return self;
}

@end
