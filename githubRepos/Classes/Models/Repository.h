//
//  Repository.h
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Repository : NSObject

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSString *info;

@property (nonatomic, copy, readonly) NSString *ownerName;
@property (nonatomic, copy, readonly) NSString *ownerIcon;

@property (nonatomic, copy, readonly) NSString *branch;
@property (nonatomic, copy, readonly) NSString *commit;
@property (nonatomic, copy, readonly) NSString *hashCommit;

- (instancetype)initWithDict:(NSDictionary*)dict;

@end

