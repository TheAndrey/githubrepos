//
//  NetworkManager.m
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import "NetworkManager.h"
#import <AFNetworking.h>

@interface NetworkManager ()
@property (nonatomic, strong) AFHTTPSessionManager *manager;
@end

@implementation NetworkManager

#pragma mark - Lifecycle

+ (NetworkManager*)sharedManager {
    static NetworkManager *sharedManager = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^ {
        sharedManager = [NetworkManager new];
    });
    
    return sharedManager;
}

- (id)init {
    if ((self = [super init])) {
    }
    
    _manager = [AFHTTPSessionManager manager];
    
    return self;
}

#pragma mark - Public API

- (void)loadRepositoriesWithParams:(NSDictionary *)params sucess:(SuccessBlock)success failure:(FailureBlock)failure {
    NSString *url = [NSString stringWithFormat:@"https://api.github.com/search/repositories?q=language:swift&sort=stars&order=desc&page=%@&per_page=%@", params[@"page"], params[@"per_page"]];
    
    [self.manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}


@end
