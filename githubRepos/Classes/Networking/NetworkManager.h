//
//  NetworkManager.h
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SuccessBlock)(id result);
typedef void (^FailureBlock)(NSError *error);

@interface NetworkManager : NSObject

+ (id)sharedManager;
- (void)loadRepositoriesWithParams:(NSDictionary*)params sucess:(SuccessBlock)success failure:(FailureBlock)failure;

@end
