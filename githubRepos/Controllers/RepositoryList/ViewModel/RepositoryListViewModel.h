//
//  RepositoryListViewModel.h
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "Repository.h"

@interface RepositoryListViewModel : NSObject

@property (nonatomic, readonly) RACSignal *reloadTable;

- (NSString *)title;
- (NSUInteger)numberOfRowsInSection:(NSInteger)section;
- (Repository*)repositoriAtIndexPath:(NSIndexPath*)indexPath;
- (void)willDisplayCellForRowAtIndexPath:(NSIndexPath*)indexPath;

- (void)loadRepositories;
- (void)refreshData;

@end
