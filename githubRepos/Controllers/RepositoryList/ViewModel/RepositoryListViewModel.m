//
//  RepositoryListViewModel.m
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import "RepositoryListViewModel.h"
#import "NetworkManager.h"
#import "Repository.h"

@interface RepositoryListViewModel ()

@property (nonatomic, strong) NSMutableArray<Repository *> *repositories;
@property (nonatomic, assign) NSInteger pageNumber;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger pageOffset;
@property (nonatomic, assign) BOOL isLoading;
@end

@implementation RepositoryListViewModel

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _repositories = [NSMutableArray array];
    _reloadTable = RACObserve(self, repositories);
    _pageNumber = 1;
    _pageSize = 20;
    _isLoading = NO;
    
    return self;
}

#pragma mark - Data Source

- (NSString *)title {
    return @"Repositories";
}

- (NSUInteger)numberOfRowsInSection:(NSInteger)section {
    return self.repositories.count;
}

- (Repository*)repositoriAtIndexPath:(NSIndexPath*)indexPath {
    return self.repositories[indexPath.row];
}

- (void)willDisplayCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isLoading &
        (self.repositories.count <= indexPath.row + 1) ) {
        [self loadRepositories];
    }
}

#pragma mark - Public API

- (void)loadRepositories {
    NetworkManager *manager = [NetworkManager sharedManager];
    
    @weakify(self);
    
    NSDictionary *params = @{@"page": @(self.pageNumber),
                             @"per_page": @(self.pageSize)};
    self.isLoading = YES;
    
    [manager loadRepositoriesWithParams:params sucess:^(id result) {
        @strongify(self);
        self.isLoading = NO;
        
        NSArray *items = result[@"items"];
        
        if ([items isKindOfClass:[NSArray class]]) {
            self.pageNumber += 1;
            for (NSDictionary *dict in items) {
                Repository *repository = [[Repository alloc] initWithDict:dict];
                [self.repositories addObject:repository];
            }
            RAC(self, repositories) = [RACSignal return: self.repositories];
        } else {
            // show error
        }
    } failure:^(NSError *error) {
        self.isLoading = NO;
        // show error
    }];
}

- (void)refreshData {
    if (!self.isLoading) {
        self.repositories = [NSMutableArray array];
        self.pageNumber = 1;
        [self loadRepositories];
    }
}

@end
