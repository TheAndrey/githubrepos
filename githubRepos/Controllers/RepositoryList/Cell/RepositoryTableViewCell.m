//
//  RepositoryTableViewCell.m
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import "RepositoryTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RepositoryTableViewCell ()
@property (nonatomic, weak) IBOutlet UILabel *repoNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *ownerNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *repoDescriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *ownerIconImageView;

@end

@implementation RepositoryTableViewCell

+(NSString*)reuseIdentifier {
    return NSStringFromClass([self class]);
}

-(void) prepareForReuse {
    [super prepareForReuse];
    self.repoNameLabel.text = nil;
    self.ownerNameLabel.text = nil;
    self.repoDescriptionLabel.text = nil;
    self.ownerIconImageView.image = nil;
}

- (void)updateWith:(Repository *)repository {
    self.repoNameLabel.text = repository.name;
    self.ownerNameLabel.text = repository.ownerName;
    self.repoDescriptionLabel.text = repository.info;
    [self.ownerIconImageView sd_setImageWithURL: [NSURL URLWithString:repository.ownerIcon]];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
