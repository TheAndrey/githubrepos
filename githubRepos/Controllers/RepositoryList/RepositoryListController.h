//
//  RepositoryListController.h
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoryListViewModel.h"

@interface RepositoryListController : UIViewController

- (instancetype)initWithViewModel:(RepositoryListViewModel *)viewModel;

@end
