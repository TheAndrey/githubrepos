//
//  RepositoryListController.m
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import "RepositoryListController.h"
#import "RepositoryTableViewCell.h"
#import "RepositoryDetailViewModel.h"
#import "RepositoryDetailController.h"

@interface RepositoryListController ()
@property (nonatomic, strong, readonly) RepositoryListViewModel *viewModel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end

@implementation RepositoryListController

#pragma mark - Lifecycle

- (instancetype)initWithViewModel:(RepositoryListViewModel *)viewModel {
    self = [super initWithNibName:nil bundle:nil];
    
    if (!self)
        return nil;
    
    _viewModel = viewModel;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self viewModelBindings];
    
    self.title = self.viewModel.title;
    
    [self.tableView registerNib:[UINib nibWithNibName:RepositoryTableViewCell.reuseIdentifier bundle:nil]
         forCellReuseIdentifier:RepositoryTableViewCell.reuseIdentifier];
    
    [self.viewModel loadRepositories];
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [self.tableView setRefreshControl:self.refreshControl];
}

#pragma mark - Bindings

- (void)viewModelBindings {
    @weakify(self);
    
    [[self.viewModel.reloadTable deliverOnMainThread] subscribeNext:^(id _) {
        @strongify(self);
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RepositoryTableViewCell.reuseIdentifier
                                                                    forIndexPath:indexPath];
    Repository *repository = [self.viewModel repositoriAtIndexPath:indexPath];
    [cell updateWith:repository];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Repository *repository = [self.viewModel repositoriAtIndexPath:indexPath];
    RepositoryDetailViewModel *viewModel = [[RepositoryDetailViewModel alloc] initWith: repository];
    RepositoryDetailController *controller = [[RepositoryDetailController alloc] initWithViewModel:viewModel];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.viewModel willDisplayCellForRowAtIndexPath:indexPath];
}

#pragma mark - Publik API

- (void)refreshTable {
    [self.viewModel refreshData];
}

@end
