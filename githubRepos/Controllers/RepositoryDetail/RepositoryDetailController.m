//
//  RepositoryDetailController.m
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import "RepositoryDetailController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RepositoryDetailController ()
@property (nonatomic, strong, readonly) RepositoryDetailViewModel *viewModel;
@property (nonatomic, weak) IBOutlet UILabel *ownerNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *commitHashLabel;
@property (nonatomic, weak) IBOutlet UILabel *commitNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *branchLabel;
@property (nonatomic, weak) IBOutlet UIImageView *ownerIcon;
@end

@implementation RepositoryDetailController

#pragma mark - Lifecycle

- (instancetype)initWithViewModel:(RepositoryDetailViewModel *)viewModel {
    self = [super initWithNibName:nil bundle:nil];
    
    if (!self)
        return nil;
    
    _viewModel = viewModel;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateCurrentView];
}

- (void)updateCurrentView {
    self.ownerNameLabel.text = self.viewModel.ownerName;
    self.commitHashLabel.text = self.viewModel.commitHash;
    self.commitNameLabel.text = self.viewModel.commitName;
    self.branchLabel.text = self.viewModel.branch;
    
    [self.ownerIcon sd_setImageWithURL: [NSURL URLWithString: self.viewModel.ownerIcon]];
}

@end
