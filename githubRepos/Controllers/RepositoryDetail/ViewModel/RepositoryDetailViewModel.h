//
//  RepositoryDetailViewModel.h
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Repository.h"

@interface RepositoryDetailViewModel : NSObject

- (instancetype)initWith:(Repository*)repository;

- (NSString*)ownerName;
- (NSString*)commitHash;
- (NSString*)commitName;
- (NSString*)branch;
- (NSString*)ownerIcon;

@end
