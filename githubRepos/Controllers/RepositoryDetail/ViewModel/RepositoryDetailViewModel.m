//
//  RepositoryDetailViewModel.m
//  githubRepos
//
//  Created by andrey rulev on 25/02/2019.
//  Copyright © 2019 andrey rulev. All rights reserved.
//

#import "RepositoryDetailViewModel.h"

@interface RepositoryDetailViewModel ()

@property (nonatomic, strong) Repository *repository;

@end

@implementation RepositoryDetailViewModel

#pragma mark - Lifecycle

- (instancetype)initWith:(Repository*)repository {
    self = [super init];
    if (!self) return nil;
    
    _repository = repository;
    
    return self;
}

#pragma mark - Public API

- (NSString*)ownerName {
    return self.repository.ownerName;
}

- (NSString*)commitHash {
    return self.repository.hashCommit;
}

- (NSString*)commitName {
    return self.repository.commit;
}

- (NSString*)branch {
    return self.repository.branch;
}

- (NSString*)ownerIcon {
    return self.repository.ownerIcon;
}

@end
